<?php

namespace App\Traits;

use App\Constants\EventTypes;
use App\Events\AdminNotification;
use App\Events\FollowerNotification;
use App\Events\GuestsNotification;
use App\Events\HostsNotification;
use App\Events\PrivateMessage;
use App\Events\UserNotification;
use App\Models\Admin;
use App\Models\FixedNotification;
use App\Models\Notification;
use Illuminate\Support\Facades\Log;

trait NotificationTrait
{
    public function notifyHost($notifiable, $event, $relatedResource, $params = null, $sender = null, $data = null, $channel = "pusher")
    {
        $notificationData = [];
        $notificationData['extraData'] = $data;
        $notificationData['resource_id'] = $relatedResource ? $relatedResource->id : null;
        $body = $this->getNotificationMessage($event, EventTypes::HOST, $params ?? []);
        $notification = new Notification();
        $notification->title = $sender ? $sender->full_name : env('APP_NAME');
        $notification->body = $body;
        $notification->event = $event;
        $notification->event_type = EventTypes::HOST;
        $notification->sender_id = $sender ? $sender->id : null;
        $notification->notifiable_id = $notifiable->id;
        $notification->notifiable_type = get_class($notifiable);
        $notification->data = $notificationData;
        $notification->save();
        HostsNotification::dispatch($notification);
    }

    public function notifyGuest($notifiable, $event, $relatedResource = null, $params = null, $sender = null, $data = null, $channel = "pusher")
    {
        $notificationData = [];
        $notificationData['extraData'] = $data;
        $notificationData['resource_id'] = $relatedResource ? $relatedResource->id : null;
        $body = $this->getNotificationMessage($event, EventTypes::GUEST, $params ?? []);
        $notification = new Notification();
        $notification->title = $sender ? $sender->full_name : env('APP_NAME');
        $notification->body = $body;
        $notification->event = $event;
        $notification->event_type = EventTypes::GUEST;
        $notification->sender_id = $sender ? $sender->id : null;
        $notification->notifiable_id = $notifiable->id;
        $notification->notifiable_type = get_class($notifiable);
        $notification->data = $notificationData;
        $notification->save();
        GuestsNotification::dispatch($notification);
    }

    public function notifyUser($notifiable, $event, $relatedResource = null, $params = null, $sender = null, $data = null, $channel = "pusher")
    {
        $notificationData = [];
        $notificationData['extraData'] = $data;
        $notificationData['resourceId'] = $relatedResource ? $relatedResource->id : null;
        $body = $this->getNotificationMessage($event, EventTypes::USER, $params ?? []);
        $notification = new Notification();
        $notification->title = $sender ? $sender->full_name : env('APP_NAME');
        $notification->body = $body;
        $notification->event = $event;
        $notification->event_type = EventTypes::USER;
        $notification->sender_id = $sender ? $sender->id : null;
        $notification->notifiable_id = $notifiable->id;
        $notification->notifiable_type = get_class($notifiable);
        $notification->data = $notificationData;
        $notification->save();
        UserNotification::dispatch($notification);
    }

    public function notifyAdmin($event, $relatedResource = null, $params = null, $sender = null, $data = null, $channel = "pusher")
    {
        $notificationData = [];
        $notificationData['extraData'] = $data;
        $notificationData['resource_id'] = $relatedResource ? $relatedResource->id : null;
        $body = $this->getNotificationMessage($event, EventTypes::ADMIN, $params ?? []);
        foreach (Admin::all() as $notifiable) {
            $notification = new Notification();
            $notification->title = $sender ? $sender->full_name : env('APP_NAME');
            $notification->body = $body;
            $notification->event = $event;
            $notification->event_type = EventTypes::ADMIN;
            $notification->sender_id = $sender ? $sender->id : null;
            $notification->notifiable_id = $notifiable->id;
            $notification->notifiable_type = get_class($notifiable);
            $notification->data = $notificationData;
            $notification->save();
            AdminNotification::dispatch($notification);
        }

    }

    public function notifyFollowers($event, $relatedResource = null, $params = null, $sender = null, $data = null, $channel = "pusher")
    {
        $notificationData = [];
        $notificationData['extraData'] = $data;
        $notificationData['resource_id'] = $relatedResource->id;
        $followers = $relatedResource->user->followers;
        $body = $this->getNotificationMessage($event, EventTypes::FOLLOWERS, $params ?? []);
        foreach ($followers as $notifiable) {
            $notification = new Notification();
            $notification->title = $sender ? $sender->full_name : env('APP_NAME');
            $notification->body = $body;
            $notification->event = $event;
            $notification->event_type = EventTypes::FOLLOWERS;
            $notification->sender_id = $sender ? $sender->id : null;
            $notification->notifiable_id = $notifiable->id;
            $notification->notifiable_type = get_class($notifiable);
            $notification->data = $notificationData;
            $notification->save();
            FollowerNotification::dispatch($notification);
        }

    }

    public function getNotificationMessage($event, $type, $params)
    {

        $notificationModel = FixedNotification::where(['event_key' => $event, 'event_type' => $type])->first();
        $notification = [];

        foreach ($notificationModel->getTranslations('body') as $lang => $translation) {
            $notification[$lang] = $translation;
            foreach ($params as $key => $value) {
                $notification[$lang] = str_replace($key, $value, $translation);
            }
        }
        return $notification;

    }

}
