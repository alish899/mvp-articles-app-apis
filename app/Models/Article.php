<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;



class Article extends AppModel
{
    protected $table = "articles";
    protected $fillable = ['title','content','user_id'];
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
