<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Carbon\CarbonInterface;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UserResource extends JsonResource
{
    private $accessToken;

    public function __construct($resource, $accessToken = null)
    {
        parent::__construct($resource);
        $this->accessToken = $accessToken;

    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'username' => $this->username,
            'access_token' => $this->when(isset($this->accessToken), $this->accessToken),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
