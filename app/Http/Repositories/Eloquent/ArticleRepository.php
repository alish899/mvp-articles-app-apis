<?php

namespace App\Http\Repositories\Eloquent;

use App\Http\Repositories\IRepositories\IArticleRepository;
use App\Models\Article;
use App\Models\User;


class ArticleRepository extends BaseRepository implements IArticleRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return Article::class;
    }

}
