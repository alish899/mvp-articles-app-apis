<?php

namespace App\Http\Repositories\Eloquent;

use App\Http\Repositories\IRepositories\IUserRepository;
use App\Models\BlockedUsers;
use App\Models\Followers;
use App\Models\User;
use Illuminate\Support\Facades\Log;


class UserRepository extends BaseRepository implements IUserRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }
    /**
     * @param User $user
     * @param User $anotherUser
     * @return bool
     */
    public function checkIfUserFollowsAnother(User $user, User $anotherUser)
    {
        $blockIds = Followers::where(['follower_id' => $user->id, 'user_id' => $anotherUser->id])->get();
        if (count($blockIds)) return true;
        return false;
    }

}
