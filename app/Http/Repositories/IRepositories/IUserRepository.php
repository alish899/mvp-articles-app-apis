<?php


namespace App\Http\Repositories\IRepositories;

use App\Models\User;

interface IUserRepository extends IBaseRepository
{
    public function checkIfUserFollowsAnother(User $user, User $anotherUser);

}
