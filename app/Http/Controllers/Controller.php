<?php

namespace App\Http\Controllers;

use App\Helpers\Constants;
use App\Helpers\Mapper;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $perPage;
    public $paginate;
    public $requestData;
    public $deviceId;

    public function __construct()
    {
        $this->requestData = Mapper::toUnderScore(\Request()->all());
        $this->perPage =  $this->requestData[Constants::PER_PAGE] ?? 10;
        $this->paginate = isset($this->requestData[Constants::PAGINATE]) && filter_var($this->requestData[Constants::PAGINATE], FILTER_VALIDATE_BOOLEAN) ? filter_var($this->requestData[Constants::PAGINATE], FILTER_VALIDATE_BOOLEAN) : true;
        $this->deviceId = request()->header('deviceId') ?? null;
    }
}
