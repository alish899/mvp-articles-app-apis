<?php

namespace App\Http\Controllers\API\Client\Auth;

use App\Helpers\EmailHelper;
use App\Helpers\Mapper;
use App\Helpers\ResponseStatus;
use App\Helpers\TwillowHelper;
use App\Helpers\ValidatorHelper;
use App\Http\Repositories\IRepositories\IAdminRepository;
use App\Http\Repositories\IRepositories\IPasswordResetRepository;
use App\Http\Repositories\IRepositories\IUserRepository;
use App\Http\Resources\UserResource;
use App\Jobs\SendEmail;
use App\Mail\ResetPassword;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Helpers\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class PasswordResetController extends Controller
{
    protected $userRepository;
    protected $passwordResetRepository;
    use EmailHelper, TwillowHelper;

    public function __construct(IPasswordResetRepository $passwordResetRepository, IUserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->passwordResetRepository = $passwordResetRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResetOtpCode()
    {
        $validationRules = [
            'phone' => ['required_without:email', Rule::exists('users', 'phone')->where(function ($query) {
                return $query->whereNotNull('phone_verified_at');
            })],
            'email' => ['required_without:phone', Rule::exists('users', 'email')->where(function ($query) {
                //return $query->whereNotNull('email_verified_at');
            })],
            'channel' => 'required_with:phone',
        ];
        $validator = Validator::make($this->requestData, $validationRules);
        if ($validator->passes()) {
            if (isset($user->otp_code_sent_at) && $user->otp_reset_code_sent_at > Carbon::now()->subMinutes(env('TIME_TO_RESEND_OTP', 30)))
                return JsonResponse::respondError(JsonResponse::MSG_WAIT_BEFORE_RESEND_OTP_CODE, ResponseStatus::RESEND_OTP_WAIT);
            $key = isset($this->requestData['phone']) ? "phone" : "email";
            $user = $this->userRepository->findBy($key, $this->requestData[$key]);
            if (!$user) return JsonResponse::respondError(JsonResponse::MSG_USER_NOT_FOUND);
            if ($key == "phone") {
                if (!env('TWILIO_TEST_MODE'))
                    $this->sendVerificationSmsORCall($this->requestData['phone'], $this->requestData['channel']);
            } else
                $this->sendResetCodeEmail($user);
            $this->userRepository->update(['otp_reset_code_sent_at'=>Carbon::now()],$user->id);
            return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS);
        }
        return JsonResponse::respondError($validator->errors()->all());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Twilio\Exceptions\ConfigurationException
     * @throws \Twilio\Exceptions\TwilioException
     */
    public function verifyTwilloResetOtpCode()
    {
        $data = $this->requestData;
        $validation_rules = [
            'phone' => ['required_without:email', Rule::exists('users', 'phone')->where(function ($query) {
                return $query->whereNotNull('phone_verified_at');
            })],
            'otp_code' => 'required|string',
        ];
        $validator = Validator::make($data, $validation_rules, ValidatorHelper::messages());
        if ($validator->passes()) {
            $codeVerified = env('TWILIO_TEST_MODE') ?? $this->verifyOtpCode($data['phone'], $data['otp_code']);
            if ($codeVerified) {
                $user = $this->userRepository->findBy('phone', $data['phone']);
                if ($user) {
                    $token = $user->createToken($this->deviceId)->plainTextToken;
                    $user->refresh();
                    return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS, ['access_token' => $token]);
                } else {
                    return JsonResponse::respondError(JsonResponse::MSG_GENERAL_ERROR, ResponseStatus::GENERAL_ERROR);
                }
            } else return JsonResponse::respondError(JsonResponse::MSG_OTP_CODE_INVALID, ResponseStatus::OTP_CODE_INVALID);
        } else {
            return JsonResponse::respondError($validator->errors()->all(), ResponseStatus::VALIDATION_ERROR);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyEmailResetOtpCode(): \Illuminate\Http\JsonResponse
    {
        $data = $this->requestData;
        $rules = [
            'email' => ['required_without:phone', Rule::exists('users', 'email')->where(function ($query) {
            })],
            'otp_code' => 'required|string'
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {
            $user = $this->userRepository->findBy('email',$data['email']);
            if ($this->verifyResetCode($user, $data['otp_code'])) {
                $token = $user->createToken($this->deviceId)->plainTextToken;
                $user->refresh();
                return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS,['access_token' => $token], ResponseStatus::SUCCESS);
            } else
                return JsonResponse::respondError(JsonResponse::MSG_EMAIL_VERIFY_CODE_INVALID);

        } else
            return JsonResponse::respondError($validator->errors(),ResponseStatus::VALIDATION_ERROR);
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword()
    {
        try {
            $rules = [
                'password' => 'required|string',
                'password_confirmation' => 'required|same:password',
            ];

            $validator = Validator::make($this->requestData, $rules);
            if ($validator->passes()) {
                $user = Auth::guard('client')->user();
                $this->userRepository->update(['password' => bcrypt($this->requestData['password'])], $user->id);
                return JsonResponse::respondSuccess(trans(JsonResponse::MSG_SUCCESS));
            } else {
                return JsonResponse::respondError($validator->errors());
            }
        } catch (\Exception $exception) {
            return JsonResponse::respondError($exception->getMessage());
        }

    }


}
