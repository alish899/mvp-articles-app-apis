<?php

namespace App\Http\Controllers\API\Client\Auth;


use App\Exceptions\GeneralException;
use App\Helpers\Constants;
use App\Helpers\Helpers;
use App\Helpers\Mapper;
use App\Helpers\JsonResponse;
use App\Helpers\ResponseStatus;
use App\Helpers\TwillowHelper;
use App\Helpers\ValidatorHelper;
use App\Http\Controllers\Controller;
use App\Http\Repositories\IRepositories\ICountryRepository;
use App\Jobs\Messages\SendSinglePushNotification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Repositories\IRepositories\IUserRepository;
use Illuminate\Validation\Rule;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Factory;
use Twilio\Exceptions\RestException;
use Twilio\Rest\Client;

class VerifyController extends Controller
{
    public function VerifyEmail($token = null)
    {
        if($token == null) {

            session()->flash('message', 'Invalid Login attempt');

            return redirect()->route('login');

        }

        $user = User::where('email_verification_token',$token)->first();

        if($user == null ){

            session()->flash('message', 'Invalid Login attempt');

            return redirect()->route('login');

        }

        $user->update([

            'email_verified' => 1,
            'email_verified_at' => Carbon::now(),
            'email_verification_token' => ''

        ]);

        session()->flash('message', 'Your account is activated, you can log in now');

        return redirect()->route('login');

    }

}
