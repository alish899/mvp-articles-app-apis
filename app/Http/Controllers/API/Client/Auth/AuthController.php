<?php

namespace App\Http\Controllers\API\Client\Auth;


use Adldap\AdldapInterface;
use Adldap\Laravel\Facades\Adldap;
use App\Constants\Events;
use App\Exceptions\GeneralException;
use App\Helpers\Constants;
use App\Helpers\EmailHelper;
use App\Helpers\JsonResponse;
use App\Helpers\ResponseStatus;
use App\Helpers\TwillowHelper;
use App\Helpers\ValidatorHelper;
use App\Http\Controllers\Controller;
use App\Http\Repositories\IRepositories\ICountryRepository;
use App\Http\Repositories\IRepositories\IFcmNotificationRepository;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\NotificationTrait;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Repositories\IRepositories\IUserRepository;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Kreait\Firebase\Factory;

class AuthController extends Controller
{
    protected $userRepository;
    protected $factory;
    private $fcmNotificationRepository;
    protected $messaging;

    use TwillowHelper, EmailHelper, NotificationTrait;

    protected $ldap;

    public function __construct(
        IUserRepository $userRepository
    )
    {
        parent::__construct();

        $this->userRepository = $userRepository;
    }

    /**
     * @desc login
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $data = $this->requestData;
        $validation_rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($data, $validation_rules, ValidatorHelper::messages());
        if ($validator->passes()) {
                $user = User::where('username', $data['username'])->first();
                if ($user) {
                    $deviceId = request()->header(Constants::HEADER_DEVICE_ID_KEY) ?? Str::random(8);
                    $token = $user->createToken($deviceId)->plainTextToken;
                    $user['access_token'] = $token;
                    return JsonResponse::respondSuccess(JsonResponse::MSG_LOGIN_SUCCESSFULLY, new UserResource($user, $token));
                }
        }
         return JsonResponse::respondError(JsonResponse::MSG_FAILED, ResponseStatus::NOT_AUTHENTICATED);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @desc get authenticated user
     * @author ali.shaaban
     */
    public
    function getUser()
    {
        $user = Auth::guard('client')->user();
        return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS, new UserResource($user));
    }

    /**
     * logout and revoke user tokens
     * @return \Illuminate\Http\JsonResponse
     * @author ali shaaban
     */
    public
    function logout()
    {

        $user = Auth::guard('client')->user();
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
        return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS);
    }

}
