<?php

namespace App\Http\Controllers\API\Client\Auth;

use App\Helpers\EmailHelper;
use App\Helpers\Mapper;
use App\Helpers\ResponseStatus;
use App\Http\Repositories\IRepositories\IAdminRepository;
use App\Http\Repositories\IRepositories\IEmailVerifyRepository;
use App\Http\Repositories\IRepositories\IPasswordResetRepository;
use App\Http\Repositories\IRepositories\IUserRepository;
use App\Jobs\SendEmail;
use App\Mail\ResetPassword;
use App\Mail\VerifyCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Helpers\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class EmailVerifyController extends Controller
{
    public $userRepository;
    public $emailVerifyRepository;
    public $authUser;
    use EmailHelper;

    public function __construct(IEmailVerifyRepository $emailVerifyRepository, IUserRepository $userRepository)
    {
        parent::__construct();
        $this->emailVerifyRepository = $emailVerifyRepository;
        $this->authUser = Auth::guard('client')->user();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendVerifyCode(): \Illuminate\Http\JsonResponse
    {
        $this->sendVerifyCodeEmail($this->authUser);
        return JsonResponse::respondSuccess(JsonResponse::MSG_SMS_CODE_SENT_SUCCESSFULLY, ResponseStatus::SUCCESS);
    }






}
