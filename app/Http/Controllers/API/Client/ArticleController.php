<?php

namespace App\Http\Controllers\API\Client;

use App\Helpers\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Repositories\IRepositories\IArticleRepository;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;


class ArticleController extends Controller
{
    private $articleRepository;
    private  $authUser;
    public function __construct(IArticleRepository  $articleRepository
    )
    {
        parent::__construct();
        $this->articleRepository = $articleRepository;
        $this->authUser = Auth::guard('client')->user() ?? null;

    }
    public function getArticles(){

        return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS,  ArticleResource::collection($this->articleRepository->all()));
    }
    public function getArticleById(Article $article){

        return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS,new ArticleResource($article));
    }
    public function createArticle(){
        $data = $this->requestData;
        $article = $this->articleRepository->create(
            [
                'title' => $data['title'],
                'content' => $data['content'],
                'user_id' => $this->authUser->id
            ]);
        return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS,new ArticleResource($article));
    }

}
