<?php

namespace App\Http\Controllers\API\Client;

use App\Helpers\FirebaseHelper;
use App\Helpers\JsonResponse;

use App\Http\Controllers\Controller;
use App\Http\Repositories\IRepositories\IUserRepository;

use App\Http\Resources\UserResource;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    private $userRepository;
    private $authUser;

    public function __construct(IUserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->authUser = Auth::guard('client')->user() ?? null;
    }

    /**
     * get user info
     * @return \Illuminate\Http\JsonResponse
     * @author ali shaaban
     */
    public function userInfo()
    {
        $user = $this->authUser;
        $user->load('identity', 'identity.images', 'listings', 'users_reviews');
        return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS, new UserResource($user));
    }

}
