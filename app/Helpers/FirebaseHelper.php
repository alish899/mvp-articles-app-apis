<?php

namespace App\Helpers;

use App\Http\Repositories\IRepositories\ICountryRepository;
use App\Http\Repositories\IRepositories\IEmailVerifyRepository;
use App\Http\Repositories\IRepositories\IFcmNotificationRepository;
use App\Http\Repositories\IRepositories\IUserRepository;
use App\Jobs\SendEmail;
use App\Mail\VerifyCode;
use App\Models\EmailVerify;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Kreait\Firebase\Factory;

trait FirebaseHelper
{

    protected $factory;

    public function __construct()
    {
        $this->factory = (new Factory)->withServiceAccount(base_path(env("FIREBASE_CREDENTIALS")));
    }

    /**
     * @param $user
     * @return mixed|null
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getCurrentUserToken($user)
    {
        $token = request()->hasHeader(Constants::HEADER_DEVICE_ID_KEY)?$user->fcmTokens()->where('device_id', request()->header(Constants::HEADER_DEVICE_ID_KEY))->first():null;
        return ($token) ? $token->fcm_token : null;
    }

    /**
     * @param $user
     * @param $topicName
     * @return void
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function topicSubscribe($user, $topicName)
    {
        $factory = (new Factory)->withServiceAccount(base_path(env("FIREBASE_CREDENTIALS")));
        $messaging = $factory->createMessaging();
        $messaging->subscribeToTopic($topicName, $this->getCurrentUserToken($user));
    }

    /**
     * @param $user
     * @param $topicName
     * @return void
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function topicUnsubscribe($user, $topicName)
    {
        $factory = (new Factory)->withServiceAccount(base_path(env("FIREBASE_CREDENTIALS")));
        $messaging = $factory->createMessaging();
        $messaging->unsubscribeFromTopic($topicName, $this->getCurrentUserToken($user));
    }
}
