<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Twilio\Rest\Client;
use function Illuminate\Events\queueable;

trait LocationHelper
{
    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public function distanceBetweenTwoPoints(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }

    public function userLocationfromIp()
    {
        //$visitorIP = $_SERVER['REMOTE_ADDR'];
        $visitorIP = "84.247.59.12";
        $apiURL = 'https://api.ipbase.com/json/' . $visitorIP;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $apiResponse = curl_exec($ch);
        curl_close($ch);
        $ipInfo = json_decode($apiResponse, true);
        $location = [];
        if (!empty($ipInfo)) {

            $country_code = $ipInfo['country_code'] ?? null;
            $country_name = $ipInfo['country_name'] ?? null;
            $region_name = $ipInfo['region_name'] ?? null;
            $region_code = $ipInfo['region_code'] ?? null;
            $city = $ipInfo['city'] ?? null;
            $zip_code = $ipInfo['zip_code'] ?? null;
            $location['latitude'] = $ipInfo['latitude'] ?? null;
            $location['longitude'] = $ipInfo['longitude'] ?? null;
            $time_zone = $ipInfo['time_zone'] ?? null;
        }
        return $location;
    }
}
