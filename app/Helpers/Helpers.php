<?php



if (!function_exists("configSetting")) {
    /**
     * @param $key
     * @param $default
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    function configSetting($key,$default = null)
    {
        return config(\App\Models\Setting::SETTING_CONFIG . $key);
    }

}




