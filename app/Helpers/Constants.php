<?php


namespace App\Helpers;


class Constants
{
    // filter value
    const NULL = "null";
    const NOT_NULL = "not null";
    const PER_PAGE_DEFAULT = 10;

    const DESIGNER = "designer";
    const CLIENT = "client";
    const SUPERVISOR = "superVisor";
    const LIST_MODIFICATIONS = 'list modifications';
    const CONV_CAMEL = 'camel';
    const CONV_UNDERSCORE = 'underscore';
    const ORDER_BY = "order_by";
    const ORDER_By_DIRECTION = "order_by_direction";
    const FILTER_OPERATOR = "filter_operator";
    const FILTERS = "filters";
    const PER_PAGE = "per_page";
    const PAGINATE = "paginate";
    const HEADER_DEVICE_ID_KEY = "deviceId";

    // languages requests

    const LANGUAGE_EN = "en";
    const LANGUAGES_AR = "ar";
    const LANGUAGES_FR = "fr";
    const LANGUAGES =
        [
            Constants::LANGUAGE_EN,
            Constants::LANGUAGES_FR,
        ];
    const TOPICS_EN = [
        'recSphere_en'
    ];
    const TOPICS_AR = [
        'recSphere_ar'
    ];
    const TOPICS_FR = [
        'recSphere_fr'
    ];
    const TOPICS = [
        Constants::TOPICS_FR,
        Constants::TOPICS_EN,
        Constants::TOPICS_AR,
    ];
    const SETTING_CONFIG = "settings.";
    const SETTINGS_DATA_TYPE_STRING = "string";
    const SETTINGS_DATA_TYPE_BOOLEAN = "boolean";
    const SETTINGS_DATA_TYPE_NUMBER = "integer";
    const SETTINGS_DATA_TYPE_NUMERIC = "numeric";
    const SETTINGS_DATA_TYPE_URL = "url";


    const SETTINGS_LISTING_DISTANCE = "listing_distance";
    const SETTINGS_LISTING_MAX_IMAGES_SIZE = "listing_max_images_size";
    const SETTINGS_LISTING_MAX_IMAGES_COUNT = "listing_max_images_count";
    const SETTINGS_LISTING_MAX_VIDEOS_SIZE = "listing_max_videos_size";
    const SETTINGS_LISTING_MAX_VIDEOS_COUNT = "listing_max_videos_count";
    /////////////////////////// old constants ////////////////////////////////
    const STATUS_REJECTED = 'Rejected';
    const STATUS_ACCEPTED = 'Accepted';
    const STATUS_UNDER_REVIEW = 'Under review';
    const STATUS_UPDATE_REQUEST = 'Update request';
    const STATUSES = [
        Constants::STATUS_REJECTED,
        Constants::STATUS_ACCEPTED,
        Constants::STATUS_UNDER_REVIEW,
    ];


    const ORDER_STATUS_REJECTED = 'Rejected';
    const ORDER_STATUS_UNDER_REVIEW = 'Under review';
    const ORDER_STATUS_COMPLETED = 'Completed';
    const ORDER_STATUS_UNDERWAY = 'Underway';
    const ORDERS_STATUSES = [
        Constants::STATUS_REJECTED,
        Constants::ORDER_STATUS_COMPLETED,
        Constants::ORDER_STATUS_UNDER_REVIEW,
        Constants::ORDER_STATUS_UNDERWAY,
    ];

    const FILTER_STATUS_PENDING = 'pending';
    const FILTER_STATUS_COMPLETED = 'completed';
    const FILTER_STATUS_UNDERWAY = 'underway';
    const FILTER_STATUS_PUBLISHED = 'published';




}
