<?php

namespace App\Helpers;

use App\Http\Repositories\IRepositories\IEmailVerifyRepository;
use App\Http\Repositories\IRepositories\IUserRepository;
use App\Jobs\SendEmail;
use App\Mail\ResetPassword;
use App\Mail\VerifyCode;
use App\Models\EmailVerify;
use App\Models\PasswordReset;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

trait EmailHelper
{


    /**
     * check if code is already created within less than 24 hours,
     * then use it again, or create new code and update old one.
     * @param $user
     * @return void
     */
    public function sendVerifyCodeEmail($user)
    {
        $verify = EmailVerify::where('email', $user->email)->first();
        if ($verify) {
            if (Carbon::now()->diffInHours($verify->created_at) > 24) {
                $verifyCode = Str::random(4);
                $verify->verify_code = $verifyCode;
                $verify->save();
            } else {
                $verifyCode = $verify->verify_code;
            }
        } else {
            $verifyCode = Str::random(4);
            EmailVerify::create(['email' => $user->email, 'verify_code' => $verifyCode]);
        }
        $name = $user->first_name;
        $verifyMail = new VerifyCode([
            'name' => $name,
            'verifyCode' => $verifyCode,
        ]);
        SendEmail::dispatch($verifyMail, $user->email);

    }

    /**
     * verify code
     * update user verify status
     * delete verify record
     * @param $user
     * @param $verifyCode
     * @return bool
     */
    public function verifyCode($user, $verifyCode): bool
    {
        $verify = EmailVerify::where('email', $user->email)->first();
        if ($verify && $verifyCode === $verify->verify_code && Carbon::now()->diffInHours($verify->created_at) < 24) {
            $verify->delete();
            $user->email_verified_at = Carbon::now();
            $user->save();
            return true;
        }
        return false;
    }

    /**
     * check if code is already created within less than 24 hours,
     * then use it again, or create new code and update old one.
     * @param $user
     * @return void
     */
    public function sendResetCodeEmail($user)
    {
        $verify = PasswordReset::where('email', $user->email)->first();
        if ($verify) {
            if (Carbon::now()->diffInHours($verify->created_at) > 24) {
                $resetCode = Str::random(4);
                $verify->reset_code = $resetCode;
                $verify->save();
            } else {
                $resetCode = $verify->reset_code;
            }
        } else {
            $resetCode = Str::random(4);
            PasswordReset::create(['email' => $user->email, 'reset_code' => $resetCode]);
        }
        $name = $user->first_name;
        $resetMail = new ResetPassword([
            'name' => $name,
            'resetCode' => $resetCode,
        ]);
        SendEmail::dispatch($resetMail, $user->email);
        Log::info("reset email sent to " . $user->email . " at " . Carbon::now());
    }

    /**
     * verify code
     * update user verify status
     * delete verify record
     * @param $user
     * @param $verifyCode
     * @return bool
     */
    public function verifyResetCode($user, $resetCode): bool
    {
        $verify = PasswordReset::where('email', $user->email)->first();
        if ($verify && $resetCode === $verify->reset_code && Carbon::now()->diffInHours($verify->created_at) < 24) {
            $verify->delete();
            return true;
        }
        return false;
    }
}
