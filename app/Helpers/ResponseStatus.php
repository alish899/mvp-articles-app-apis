<?php


namespace App\Helpers;


class ResponseStatus
{
    const NOT_FOUND = 404;
    const NOT_ALLOWED= 405 ;
    const NOT_AUTHORIZED = 403;
    const NOT_AUTHENTICATED = 401;
    const ACCESS_FORBIDDEN = 403;
    const BAD_REQUEST = 400;
    const NO_POLL_REQUEST = 600;
    const VALIDATION_ERROR = 422;
    const GENERAL_ERROR = 500;
    const CREATED = 201;
    const SUCCESS = 200;
    const OTP_CODE_INVALID = 488;
    const RESEND_OTP_WAIT = 487;
    const DEVICE_ID_MISSING = 477;
    const USER_NOT_VERIFIED = 476;
    const MAX_IMAGES_SIZE_REACHED = 455;
    const MAX_IMAGES_COUNT_REACHED = 456;
    const MAX_VIDEOS_SIZE_REACHED = 457;
    const MAX_VIDEOS_COUNT_REACHED = 458;
    const INVALID_TIMEFRAME = 459;
    const INVALID_TOKEN = 463;
    const PAYMENT_ERROR = 460;
    const NO_CONNECTED_ACCOUNT = 466;
    /**
     * @return array
     * @desc this function is return all success response status
     * you may use it to check if this array contains any response status
     * @example
     * dd(in_array(200 , ResponseStatus::getSuccessStatus()));
     * @author karam mustafa
     */
    public static function getSuccessStatus(){
        return [200 , 201];
    }
}
