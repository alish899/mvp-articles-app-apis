<?php

namespace App\Helpers;

use Twilio\Rest\Client;

trait TwillowHelper
{
    /**
     * @param $phone
     * @param $channel
     * @return \Twilio\Rest\Verify\V2\Service\VerificationInstance
     * @throws \Twilio\Exceptions\ConfigurationException
     * @throws \Twilio\Exceptions\TwilioException
     */
    public function sendVerificationSmsORCall($phone, $channel)
    {
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        return $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create($phone, $channel);
    }

    /**
     * @param $phone
     * @param $otpCode
     * @return bool
     * @throws \Twilio\Exceptions\ConfigurationException
     * @throws \Twilio\Exceptions\TwilioException
     */
    public function verifyOtpCode($phone, $otpCode)
    {
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $verification = $twilio->verify->v2->services($twilio_verify_sid)
            ->verificationChecks
            ->create($otpCode, array('to' => $phone));
        return $verification->valid ?? false;
    }

}
