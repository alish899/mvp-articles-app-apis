<?php

namespace App\Constants;
class EventTypes
{
    const HOST = "host";
    const GUEST = "guest";
    const ADMIN = "admin";
    const USER = "user";
    const FOLLOWERS = "followers";


}
