<?php

namespace App\Constants;
class EventParams
{
    const EVENT_GUEST_NAME_PARAM = 'guest_name';
    const EVENT_HOST_NAME_PARAM = 'host_name';
    const EVENT_RESOURCE_ID_PARAM = 'resource_id';

}
