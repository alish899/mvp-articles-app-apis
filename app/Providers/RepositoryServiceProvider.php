<?php

namespace App\Providers;
use App\Http\Repositories\Eloquent\ArticleRepository;
use App\Http\Repositories\Eloquent\UserRepository;
use App\Http\Repositories\IRepositories\IArticleRepository;
use App\Http\Repositories\IRepositories\IUserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IUserRepository::class, UserRepository::class);
        $this->app->bind(IArticleRepository::class, ArticleRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
