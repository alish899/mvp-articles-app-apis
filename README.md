
# 🌟 MVP Article App - apis

serve create and list articles apis


## 💡 Features

- login
- logout
- Create Article for logged in user (username:Ahmad, password:123456)
- view articles for all user


## ⚙️ Installation
rename .env.example to .env

Install from composer.json
```bash
  composer install
```
Install from composer.json
```bash
  composer install
```
create new mysql database with following info:
name: mvp_app
username: root
password: ""
Run Migration
```bash
  php artisan migrate
```
Run Seeder to create test user
```bash
  php artisan db:seed
```
Run Server
```bash
  php artisan serve
```


#### using tools
- Laravel
- Mysql


## 🧍 Authors

- [Ali shaaban]

