<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Client\Auth\AuthController as ClientAuthController;
use App\Http\Controllers\API\Client\ArticleController as ArticleController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
    "prefix" => "client",
], function () {

    Route::group(["prefix" => "auth"], function () {
        Route::post('login', [ClientAuthController::class, "login"]);
        Route::group(['middleware' => ['auth:client']], function () {
            Route::get('getUser', [ClientAuthController::class, 'getUser']);
            Route::post('logout', [ClientAuthController::class, 'logout']);
        });

    });
    Route::group(["prefix" => "articles"], function () {
        Route::get('/{article}', [ArticleController::class, 'getArticleById']);
        Route::get('/', [ArticleController::class, 'getArticles']);
        Route::group(['middleware' => ['auth:client']], function () {
            Route::post('/', [ArticleController::class, 'createArticle']);
        });

    });
});
